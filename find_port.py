from zeroconf import Zeroconf

r = Zeroconf()
queried_info = r.get_service_info("s_ecammliveremote._tcp.local.", "Ecamm Live Remote._ecammliveremote._tcp.local.")
port = queried_info.port
print(f"*** Port used is {port}\n")

r.close()
