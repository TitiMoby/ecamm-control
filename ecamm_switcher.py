from zeroconf import Zeroconf
import requests
import pprint
import json


def print_scene_list(scenes, suffix):
    for scene in scenes:
        print(f"{suffix} Title: {scene['title']} / UUID: {scene['UUID']}")
        children = scene['Children']
        if len(children) > 0:
            print_scene_list(children, suffix + ' *-*')


def list_scene(scenes):
    extract = []
    for scene in scenes:
        extract.append({'title': scene['title'], 'UUID': scene['UUID'], 'group': 'Group' in scene})
        children = scene['Children']
        if len(children) > 0:
            extract.extend(list_scene(children))
    return extract


r = Zeroconf()
queried_info = r.get_service_info("s_ecammliveremote._tcp.local.", "Ecamm Live Remote._ecammliveremote._tcp.local.")
if queried_info is None:
    print("First launch Ecamm Live")
else:
    port = queried_info.port
    print(f"*** Port used is {port}\n")
    url_base = f"http://127.0.0.1:{port}"
    url_info = url_base + '/getInfo'
    response = requests.get(url_info)
    if response.status_code == 200:
        info = json.loads(response.content.decode('utf-8'))
        print('*** Info\n')
        pprint.pprint(info)

    url_scene_list = url_base + '/getSceneList'
    response = requests.get(url_scene_list)
    if response.status_code == 200:
        scene_list = json.loads(response.content.decode('utf-8'))
        print('\n*** Scene list\n')
        print_scene_list(scene_list['items'], '*-*')
    all_scenes = list_scene(scene_list['items'])

    switch_scene = input("Switch to Scene (UUID): ")
    url_switch = url_base + f"/setScene?id={switch_scene}"
    requests.get(url_switch)

r.close()
